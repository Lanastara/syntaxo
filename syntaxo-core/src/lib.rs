use nom::{
    branch::alt,
    character::complete::{
        alpha1, alphanumeric1, char, digit1, line_ending, none_of, space0, space1,
    },
    combinator::{all_consuming, complete, map, map_opt, opt, recognize, value},
    multi::{many0_count, separated_list0, separated_list1},
    sequence::{delimited, pair, preceded, separated_pair, terminated, tuple},
    IResult,
};

macro_rules! debug {
    ($fmt: literal, $($input: expr),*) => {
        // #[cfg(debug_assertions)]
        // println!($fmt, $($input),+);
    };
    ($fmt: literal) => {
        // #[cfg(debug_assertions)]
        // println!($fmt);
    };
}

#[derive(Debug, PartialEq, Eq)]
pub struct Syntax<'a> {
    pub rules: Vec<Rule<'a>>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Rule<'a> {
    pub name: &'a str,
    pub transparent: bool,

    pub token: Token<'a>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Token<'a> {
    pub invert: bool,
    pub token: InnerToken<'a>,
    pub repeat: Option<Repeat>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Repeat {
    pub min: Option<u32>,
    pub max: Option<u32>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum InnerToken<'a> {
    Constant(&'a str),
    Rule(&'a str),
    Sequence(Vec<Token<'a>>),
    Choice(Vec<Token<'a>>),
}

impl<'a> Syntax<'a> {
    pub fn parse(text: &'a str) -> Result<Self, nom::Err<nom::error::Error<&'a str>>> {
        all_consuming(complete(syntax))(text).map(|(_input, syntax)| syntax)
    }
}

fn syntax(input: &str) -> IResult<&str, Syntax<'_>> {
    debug!("syntax: {}", input);
    let (input, rules) =
        separated_list0(line_ending, alt((map(rule, Some), map(space0, |_| None))))(input)?;

    let rules = rules.into_iter().flatten().collect();

    debug!("syntax!");
    Ok((input, Syntax { rules }))
}

fn rule_name(input: &str) -> IResult<&str, &str> {
    recognize(pair(
        alpha1,
        many0_count(alt((map(alphanumeric1, |_| ()), map(char('_'), |_| ())))),
    ))(input)
}

fn rule(input: &str) -> IResult<&str, Rule<'_>> {
    debug!("fn rule: {}", input);
    let (input, name) = rule_name(input)?;
    let (input, _) = tuple((space1, char('='), space1))(input)?;
    let (input, modifier) = opt(alt((char('_'), char('@'))))(input)?;
    let (input, token) = delimited(char('{'), token, char('}'))(input)?;

    let transparent = modifier == Some('_');

    debug!("fn rule!");
    Ok((
        input,
        Rule {
            name,
            token,
            transparent,
        },
    ))
}

fn inner_token(input: &str) -> IResult<&str, InnerToken<'_>> {
    debug!("fn inner_token: {}", input);
    let (input, token) = alt((
        map(choice, InnerToken::Choice),
        map(sequence, InnerToken::Sequence),
        delimited(char('('), inner_token, char(')')),
        inner_terminal,
    ))(input)?;
    debug!("fn inner_token!");
    debug!("token: {:?}", token);

    Ok((input, token))
}

fn sequence(input: &str) -> IResult<&str, Vec<Token>> {
    debug!("fn sequence: {}", input);
    let (input, (token, mut tokens)) = delimited(
        space0,
        separated_pair(
            terminal,
            separator('~'),
            separated_list1(separator('~'), terminal),
        ),
        space0,
    )(input)?;

    tokens.insert(0, token);

    debug!("fn sequence!");
    Ok((input, tokens))
}

fn choice(input: &str) -> IResult<&str, Vec<Token>> {
    debug!("fn choice: {}", input);
    let (input, (token, mut tokens)) = delimited(
        space0,
        separated_pair(
            alt((
                map(sequence, |s| Token {
                    token: InnerToken::Sequence(s),
                    invert: false,
                    repeat: None,
                }),
                terminal,
            )),
            separator('|'),
            separated_list1(
                separator('|'),
                alt((
                    map(sequence, |s| Token {
                        token: InnerToken::Sequence(s),
                        invert: false,
                        repeat: None,
                    }),
                    terminal,
                )),
            ),
        ),
        space0,
    )(input)?;

    tokens.insert(0, token);

    debug!("fn chioce!");
    debug!("tokens: {:?}", tokens);

    Ok((input, tokens))
}

fn separator<I, Error>(c: char) -> impl FnMut(I) -> IResult<I, char, Error>
where
    I: Clone
        + nom::InputLength
        + nom::InputTake
        + nom::InputIter
        + nom::InputTakeAtPosition
        + nom::Slice<std::ops::RangeFrom<usize>>,
    Error: nom::error::ParseError<I>,
    <I as nom::InputIter>::Item: Clone + nom::AsChar,
    <I as nom::InputTakeAtPosition>::Item: Clone + nom::AsChar,
{
    delimited(space0, char(c), space0)
}

fn repeat(input: &str) -> IResult<&str, Option<Repeat>> {
    opt(alt((
        value(
            Repeat {
                min: Some(0),
                max: Some(1),
            },
            char('?'),
        ),
        value(
            Repeat {
                min: Some(0),
                max: None,
            },
            char('*'),
        ),
        value(
            Repeat {
                min: Some(1),
                max: None,
            },
            char('+'),
        ),
        delimited(
            char('{'),
            alt((
                map_opt(
                    separated_pair(digit1, char(','), digit1),
                    |(v1, v2): (&str, &str)| {
                        Some(Repeat {
                            min: Some(v1.parse().ok()?),
                            max: Some(v2.parse().ok()?),
                        })
                    },
                ),
                map_opt(terminated(digit1, char(',')), |v1: &str| {
                    Some(Repeat {
                        min: Some(v1.parse().ok()?),
                        max: None,
                    })
                }),
                map_opt(preceded(char(','), digit1), |v1: &str| {
                    Some(Repeat {
                        min: None,
                        max: Some(v1.parse().ok()?),
                    })
                }),
                map_opt(digit1, |v1: &str| {
                    Some(Repeat {
                        min: Some(v1.parse().ok()?),
                        max: Some(v1.parse().ok()?),
                    })
                }),
            )),
            char('}'),
        ),
    )))(input)
}

fn token(input: &str) -> IResult<&str, Token> {
    debug!("fn token: {}", input);
    let (input, invert) = opt(char('!'))(input)?;
    let (input, token) = inner_token(input)?;
    let (input, _) = space0(input)?;
    let (input, repeat) = repeat(input)?;

    let invert = invert.is_some();

    Ok((
        input,
        Token {
            token,
            repeat,
            invert,
        },
    ))
}

fn terminal(input: &str) -> IResult<&str, Token> {
    debug!("fn terminal: {}", input);
    let (input, invert) = opt(char('!'))(input)?;
    let (input, token) = inner_terminal(input)?;
    let (input, _) = space0(input)?;
    let (input, repeat) = repeat(input)?;

    let invert = invert.is_some();
    debug!("fn terminal!");
    debug!("token: {:?}", token);
    debug!("repeat: {:?}", repeat);

    Ok((
        input,
        Token {
            token,
            repeat,
            invert,
        },
    ))
}

fn inner_terminal(input: &str) -> IResult<&str, InnerToken> {
    debug!("fn inner_terminal: {}", input);
    let (input, token) = delimited(
        space0,
        alt((
            map(constant, InnerToken::Constant),
            map(rule_name, InnerToken::Rule),
            delimited(char('('), inner_token, char(')')),
            map(delimited(char('('), token, char(')')), |t| {
                InnerToken::Sequence(vec![t])
            }),
        )),
        space0,
    )(input)?;

    debug!("fn inner_terminal!");
    debug!("token: {:?}", token);

    Ok((input, token))
}

fn constant(input: &str) -> IResult<&str, &str> {
    debug!("fn constant: {}", input);
    let (input, constant) = delimited(
        space0,
        delimited(
            char('"'),
            recognize(many0_count(alt((
                map(none_of("\\\""), |_| ()),
                map(pair(char('\\'), char('\\')), |_| ()),
                map(pair(char('\\'), char('r')), |_| ()),
                map(pair(char('\\'), char('n')), |_| ()),
                map(pair(char('\\'), char('"')), |_| ()),
            )))),
            char('"'),
        ),
        space0,
    )(input)?;

    debug!("fn constant!");
    debug!("constant: {}", constant);

    Ok((input, constant))
}
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_whitespace() {
        let input = r#"WHITESPACE = _{ " " }"#;

        let (rest, syntax) = rule(input).unwrap();

        assert_eq!(rest, "");

        assert_eq!(
            syntax,
            Rule {
                name: "WHITESPACE",
                transparent: true,
                token: Token {
                    token: InnerToken::Constant(" "),
                    repeat: None,
                    invert: false,
                },
            },
        )
    }

    #[test]
    fn parse_newline() {
        let input = r#"NEWLINE = _{("\r\n") | "\r" | "\n"}"#;

        let (rest, syntax) = rule(input).unwrap();

        assert_eq!(rest, "");

        assert_eq!(
            syntax,
            Rule {
                name: "NEWLINE",
                transparent: true,
                token: Token {
                    token: InnerToken::Choice(vec![
                        Token {
                            token: InnerToken::Constant("\\r\\n"),
                            repeat: None,
                            invert: false,
                        },
                        Token {
                            token: InnerToken::Constant("\\r"),
                            repeat: None,
                            invert: false,
                        },
                        Token {
                            token: InnerToken::Constant("\\n"),
                            repeat: None,
                            invert: false,
                        }
                    ]),
                    repeat: None,
                    invert: false,
                }
            },
        )
    }
    #[test]
    fn parse_color_inner() {
        let input = r#"colorInner = { ASCII_HEX_DIGIT{6} ~ (ASCII_HEX_DIGIT{2})? }"#;

        let (rest, syntax) = rule(input).unwrap();

        assert_eq!(rest, "");

        assert_eq!(
            syntax,
            Rule {
                name: "colorInner",
                transparent: false,
                token: Token {
                    token: InnerToken::Sequence(vec![
                        Token {
                            token: InnerToken::Rule("ASCII_HEX_DIGIT"),
                            repeat: Some(Repeat {
                                min: Some(6),
                                max: Some(6)
                            }),
                            invert: false,
                        },
                        Token {
                            token: InnerToken::Sequence(vec![Token {
                                token: InnerToken::Rule("ASCII_HEX_DIGIT"),
                                repeat: Some(Repeat {
                                    min: Some(2),
                                    max: Some(2)
                                }),
                                invert: false,
                            }]),
                            repeat: Some(Repeat {
                                min: Some(0),
                                max: Some(1)
                            }),
                            invert: false,
                        },
                    ]),
                    repeat: None,
                    invert: false,
                }
            },
        )
    }

    #[test]
    fn parse_string_inner() {
        let input = r#"stringInner = {("\\\"" | (!"\"" ~ ANY) )*}"#;

        let (rest, syntax) = rule(input).unwrap();

        assert_eq!(rest, "");

        assert_eq!(
            syntax,
            Rule {
                name: "stringInner",
                transparent: false,
                token: Token {
                    token: InnerToken::Choice(vec![
                        Token {
                            token: InnerToken::Constant("\\\\\\\""),
                            repeat: None,
                            invert: false,
                        },
                        Token {
                            token: InnerToken::Sequence(vec![
                                Token {
                                    token: InnerToken::Constant("\\\""),
                                    repeat: None,
                                    invert: true,
                                },
                                Token {
                                    token: InnerToken::Rule("ANY"),
                                    repeat: None,
                                    invert: false,
                                }
                            ]),
                            repeat: None,
                            invert: false,
                        },
                    ]),
                    repeat: Some(Repeat {
                        min: Some(0),
                        max: None
                    }),
                    invert: false,
                }
            },
        )
    }

    #[test]
    fn parse_rules_file() {
        let input = include_str!("../../syntax.pest");

        let syntax = Syntax::parse(input).unwrap();

        dbg!(&syntax);

        // assert_eq!(syntax, Syntax { rules: vec![] })
    }
}
